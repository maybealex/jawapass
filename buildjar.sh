#!/bin/bash

# delete old jar
rm JawaPass.jar

# set up manifest
mkdir META-INF
cd META-INF

{
	echo "Manifest-Version: 1.0"
	echo "Created-By: 11.0.510"
	echo "Main-Class: Main"
	echo ""
} > MANIFEST.MF

# get class files
cd ../src
javac *.java
mv *.class ../.
cd ..

# package files
zip JawaPass.zip META-INF/MANIFEST.MF
zip JawaPass.zip *.class

# clean up
rm META-INF/MANIFEST.MF *.class
rmdir META-INF

# final touch
mv JawaPass.zip JawaPass.jar
