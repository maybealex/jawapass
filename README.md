# JawaPass

A program for Jawas, by Jawas, in Java. Humanfolk can use it too.

## Table of Contents

- [Naming](#naming)
- [Features](#features)
- [Building](#building)
- [Usage](#usage)
- [Technical Details](#techincal-details)

## Naming

Have you ever seen a Star Wars?
If you have, have you ever seen a Jawa's face?
No? Well that's because they use JawaPass to hide it.<sup>[\*](#disclaimers)</sup>

## Features

Hides:

- Hide passwords
- (Picures of) your spouse and kids
- Any other file you toss in it

Probably mostly secure, built on the technology that powers [this](https://ss64.com/pass) password generator.
Use at your own risk!

## Building

```bash
$ git clone https://gitlab.com/maybealex/jawapass
$ cd jawapass/src
$ javac Main.java
```

If you want to make a `jar` file for portability, there's a script for that.
You'll need bash.

```bash
$ cd jawapass
$ ./buildjar.sh
```

## Usage

1. Run `java Main` or `java -jar JawaPass.jar`.
2. Enter your master password, your data will be stored in your home directory inside `crypt.jawa`
3. Click the button to add some data, and choose a type.
4. Fill the fields, save, and use your secret from the list view.

## Technical Details

JawaPass was built on [Void Linux](https://voidlinux.org) x64_musl using OpenJDK11.
The IDE used is...no IDE. [Neovim](https://neovim.io) is an editor with everything needed for "serious development".

`crypt.jawa` is a base64 encoded csv file.
Why you ask?
Line readers are easy and I can't be bothered by _real_ data storage methods.

Hashes are done using the SHA256 algorithm and encryption uses AES 256.

## Disclaimers

JawaPass is not associated with OR endorsed by Disney, Lucas Film, Star Wars, or any derivative OR integral works that fall under copyright.
No money has been, is being, or will be made off of this software and any generous donations will be fed to the ~~conglomerate monster~~ wonderful, benevolent establishment of Disney.
The name is a joke, please don't sue.
