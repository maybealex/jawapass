// Gregory Dalla

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import java.awt.Container;
import java.util.ArrayList;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

// list of secrets
public class ListPanel extends JPanel {
	private boolean locked;
	private String MASTER;

	public ListPanel() {
		this.setLayout( new BoxLayout( this, BoxLayout.Y_AXIS ) );

		// MasterFrame prompt = new MasterFrame( "Enter Master Password" );
		// prompt.Test();
	}

	// refresh list of secrets
	public void update() {
		// find where user's files live
		String home = System.getProperty( "user.dir" );

		// check if they've used jawapass before
		File crypt = new File( home + "/crypt.jawa" );
		if( !crypt.exists() ) {
			try {
				FileWriter writer = new FileWriter( crypt );
				crypt.createNewFile();

				Crypt crypter = new Crypt();
				String test =  "password";
				writer.write( crypter.Base64Encode( "0,test," + crypter.EncryptString( test, MASTER ) + ";" ) );

				writer.close();
			} catch( IOException e ) {
				e.printStackTrace();
			}
		} else {
		}
	}
}
