// Gregory Dalla

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

// window to get master password
public class MasterFrame extends JFrame {
	private int[] dims;
	private MasterPanel panel;

	public MasterFrame( String windowTitle ) {
		super( windowTitle );

		this.dims = Main.getDims(); // get master dimensions

		// adjust so the new window is more "dialogue-ish"
		dims[2] *= .8;
		dims[3] *= .4;
		dims[4] = ( dims[0] / 2 ) - dims[2];
		dims[5] = ( dims[1] - dims[3] ) / 2;

		// classic jframe
		this.setSize( dims[2], dims[3] );
		this.setLocation( dims[3], dims[4] );
		this.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		this.setVisible( true );

		// add panel with content
		panel = new MasterPanel();
		this.setContentPane( panel );
	}

	public void Test() {
		System.out.println( "Testing extra frame methods" );
		panel.Test();
	}
}

// internal panel
class MasterPanel extends JPanel {
	public MasterPanel() {
	} 

	public void Test() {
		System.out.println( "Testing extra panel methods" );
	}
}
