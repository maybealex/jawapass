// Gregory Dalla

import javax.swing.JPanel;
import java.awt.BorderLayout;

// main panel
public class Content extends JPanel {
	public Content() {
		// keep things simple
		this.setLayout( new BorderLayout() );

		// two components
		JPanel list = new ListPanel(); // the list of secrets
		JPanel bar = new BarPanel(); // the bottom bar

		// add panels
		this.add( list, BorderLayout.CENTER );
		this.add( bar, BorderLayout.SOUTH );
	}
}
