// Gregory Dalla

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JFileChooser;

// create and store new secrets
public class CreationFrame extends JFrame {
	private int[] dims;

	public CreationFrame( String windowTitle ) {
		super( windowTitle );

		this.dims = Main.getDims(); // get master dimensions

		// adjust so the new window is more "dialogue-ish"
		dims[2] *= .8;
		dims[3] *= .5;
		dims[4] = ( dims[0] / 2 ) - dims[2];
		dims[5] = ( dims[1] - dims[3] ) / 2;

		// classic jframe
		this.setSize( dims[2], dims[3] );
		this.setLocation( dims[3], dims[4] );
		this.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		this.setVisible( true );

		// add panel with content
		this.setContentPane( new CreationPanel() );
	}
}

/* the is the one class that cannot really be split
 * into individual components, unfortunately
 *
 * interface for adding secrets
 */
class CreationPanel extends JPanel {
	private int step;
	private JLabel status;

	private MainPanel firstPanel, secondPanel, thirdPanel, fourthPanel, fifthPanel;

	public CreationPanel() {
		this.step = 0;
		this.setLayout( new BorderLayout() );

		// the central panel(s)
		firstPanel = new PanelOne();
		this.add( firstPanel, BorderLayout.CENTER );
		secondPanel = new PanelTwo();
		this.add( secondPanel, BorderLayout.CENTER );
		secondPanel.setVisible( false );
		thirdPanel = new PanelThree();
		this.add( thirdPanel, BorderLayout.CENTER );
		thirdPanel.setVisible( false );
		fourthPanel = new PanelFour();
		this.add( fourthPanel, BorderLayout.CENTER );
		fourthPanel.setVisible( false );
		fifthPanel = new PanelFive();
		this.add( fifthPanel, BorderLayout.CENTER );
		fifthPanel.setVisible( false );

		// action buttons
		JPanel buttons = new ButtonPanel();
		status = new JLabel( "ERROR" ); // left aligned
		buttons.add( Box.createHorizontalGlue() ); // dead spacing
		// right aligned
		JButton back = new JButton( "<- Back -" );
		back.addActionListener( new BackButton() );
		JButton forward = new JButton( "- Forward ->" );
		forward.addActionListener( new ForwardButton() );
		buttons.add( status );
		status.setVisible( false );
		buttons.add( back );
		buttons.add( forward );
		this.add( buttons, BorderLayout.SOUTH );
	}

	// show error message
	public void errorOn() {
		status.setVisible( true );
	}

	// hide error message
	public void errorOff() {
		status.setVisible( false );
	}

	// set panel
	public void setPanel( int panel ) {
		// hide old panel
		switch( step ) {
			case 0:
				firstPanel.setVisible( false );
			case 1:
				secondPanel.setVisible( false );
			case 2:
				thirdPanel.setVisible( false );
			case 3:
				fourthPanel.setVisible( false );
			case 4:
				fifthPanel.setVisible( false );
			default:
		}

		// update step
		this.step = panel;

		// show new panel
		switch( panel ) {
			case 0:
				firstPanel.setVisible( true );
			case 1:
				secondPanel.setVisible( true );
			case 2:
				thirdPanel.setVisible( true );
			case 3:
				fourthPanel.setVisible( true );
			case 4:
				fifthPanel.setVisible( true );
			default:
		}

		// re-settle components
		this.validate();
	}

	private class BackButton implements ActionListener {
		public void actionPerformed( ActionEvent e ) {
			switch( step ) {
				case 0: // secret type selection, do nothing
					break;
				case 1: // password type selection
					setPanel( 0 ); // overall selection
					break;
				case 2: // generated password
					setPanel( 1 ); // pass type selection
					break;
				case 3: // manual password
					setPanel( 1 ); // pass type selection
					break;
				case 4: // file
					setPanel( 0 ); // overall selection
					break;
				default: // uh-oh, something broke
					System.out.println( "This is an issue.\nCheck: CreationFrame.java for errors in setting step" );
					break;
			}
		}
	}

	private class ForwardButton implements ActionListener {
		public void actionPerformed( ActionEvent e ) {
			int i; // temporary variable
			switch( step ) {
				case 0: // secret type selection
					i = -1;
					try {
						i = Integer.parseInt( firstPanel.getData()[0] );
					} catch( IndexOutOfBoundsException err ) {
						errorOn();
					}

					if( i == 0 ) {
						setPanel( 1 ); // go to password type selection
					} else if( i == 1 ) {
						setPanel( 4 ); // go to file selection
					}

					errorOff();
					break;
				case 1: // password type selection
					i = -1;
					try {
						i = Integer.parseInt( secondPanel.getData()[0] );
					} catch( IndexOutOfBoundsException err ) {
						errorOn();
					}

					if( i == 0 ) {
						setPanel( 2 );
					} else if( i == 1 ) {
						setPanel( 3 );
					}

					errorOff();
					break;
				case 2: // generated password
					int type = -1;
					String name = "";
					String data = "";

					try {
						type = Integer.parseInt( thirdPanel.getData()[0] );
						name = thirdPanel.getData()[1];
						data = thirdPanel.getData()[2];
					} catch( IndexOutOfBoundsException err ) {
						errorOn();
					}

					errorOff();
					break;
				case 3: // manual password
					break;
				case 4: // file
					break;
				default: // uh-oh, something broke
					System.out.println( "This is an issue.\nCheck: CreationFrame.java for errors in setting step" );
					break;
			}
		}
	}
}

// action buttons
class ButtonPanel extends JPanel {
	public ButtonPanel() {
		this.setLayout( new BoxLayout( this, BoxLayout.LINE_AXIS ) ); // set layout
	}
}

// class to keep above methods simple
abstract class MainPanel extends JPanel {
	public abstract String[] getData();
}

class PanelOne extends MainPanel {
	private String[] choice;

	public PanelOne() {
		this.setLayout( new BoxLayout( this, BoxLayout.PAGE_AXIS ) );
	
		JLabel instructions = new JLabel( "Select which type of secret you'd like to store." );
		this.add( instructions );

		// radio buttons
		JRadioButton passButton = new JRadioButton( "Store a password" );
		passButton.addActionListener( new PassButton() );

		JRadioButton fileButton = new JRadioButton( "Store a file" );
		fileButton.addActionListener( new FileButton() );

		// button group
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add( passButton );
		buttonGroup.add( fileButton );

		this.add( passButton );
		this.add( fileButton );
	}

	public String[] getData() {
		return choice;
	}
	
	private class PassButton implements ActionListener {
		public void actionPerformed( ActionEvent e ) {
			choice = new String[] { "0" };
		}
	}
	
	private class FileButton implements ActionListener {
		public void actionPerformed( ActionEvent e ) {
			choice = new String[] { "1" };
		}
	}
}

class PanelTwo extends MainPanel {
	private String[] choice;

	public PanelTwo() {
		JLabel test = new JLabel( "two" );
		this.add( test );
	}

	public String[] getData() {
		return choice;
	}
}

class PanelThree extends MainPanel {
	private String[] data;

	public PanelThree() {
	}

	public String[] getData() {
		return data;
	}
}

class PanelFour extends MainPanel {
	private String[] data;

	public PanelFour() {
	}

	public String[] getData() {
		return data;
	}
}

class PanelFive extends MainPanel {
	private String[] data;

	public PanelFive() {
	}

	public String[] getData() {
		return data;
	}
}

// file
class FilePanel extends JPanel {
	public FilePanel() {
	}
}
