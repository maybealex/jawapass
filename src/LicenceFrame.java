// Gregory Dalla

import javax.swing.JFrame;
import javax.swing.JPanel;

public class LicenceFrame extends JFrame {
	private int[] dims;

	public LicenceFrame( String windowTitle) {
		super( windowTitle );

		this.dims = Main.getDims(); // get master dimensions

		// adjust dims
		dims[2] *= .8;
		dims[3] *= .7;
		dims[4] = ( dims[0] / 2 ) - dims[2];
		dims[5] = ( dims[1] - dims[3] ) / 2;

		// classic jframe
		this.setSize( dims[2], dims[3] );
		this.setLocation( dims[3], dims[4] );
		this.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		this.setVisible( true );

		// add some content
		this.setContentPane( new LicencePanel() );
	}
}

// shows the software licence (BSD 3-clause)
// class LicencePanel extends JPanel {
class LicencePanel extends JPanel {
	private final String LICENSE = "BSD 3-Clause License\n\nCopyright (c) 2020, maybealex\nAll rights reserved.\n\nRedistribution and use in source and binary forms, with or without\nmodification, are permitted provided that the following conditions are met:\n\n* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.\n\n* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.\n\n* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.\n\nTHIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.";

	public LicencePanel() {
	}
}
