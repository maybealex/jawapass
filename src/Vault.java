// Gregory Dalla

import java.util.ArrayList;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

// class to interact with the crypt file
public class Vault {
	private ArrayList<Secret> secrets;

	public Vault() {
		readCrypt();
		// delete file
	}

	// read crypt file into secrets
	private void readCrypt() {
		// find crypt file
		String home = System.getProperty( "user.dir" );
		File crypt = new File( home + "/crypt.jawa" );

		// temp objects
		Crypt crypter = new Crypt();
		String[] entries = new String[0];

		// split crypt by semicolons, csv spec
		try {
			entries = crypter.Base64Decode( new BufferedReader( new FileReader( crypt ) ).readLine() ).split( ";" );
		} catch( Exception e ) {
			e.printStackTrace();
		}

		// add all secrets
		for( String entry : entries ) {
			String[] values = entry.split( "," ); // csv is the best data format

			// values for secret
			int type = Integer.parseInt( values[0] );
			String name = values[1];
			String data = values[2];

			// create and add secret
			Secret secret = new Secret( type, name, data );
			secrets.add( secret );
		}
	}

	// write crypt file from secrets
	public void writeCrypt() {
	}

	// add a secret
	public void addSecret( Secret secret ) {
	}

	// get a specified secret
	public Secret getSecret( int index ) {
		try {
			return secrets.get( index + 1 );
		} catch( IndexOutOfBoundsException e ) { // only verification secret found
			return new Secret( -1, null, null );
		}
	}

	// change a secret's anything
	public void updateSecret( int index, Secret secret ) {
		secrets.set( index + 1, secret );
	}

	// delete a secret
	public void removeSecret( int index ) {
		secrets.remove( index + 1 ); // prevents removing verification entry
	}
}
