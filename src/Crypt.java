// Gregory Dalla

import java.security.MessageDigest;
import java.util.Base64;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.SecretKeyFactory;
import java.security.spec.KeySpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Cipher;

/* who knew digital cryptography could be so complicated
 * the more complex methods here were not written by me, for fear of getting it all horribly wrong
 * so here. we. GO!
 */
public class Crypt {
	// hash string with sha256
	// found on https://www.quickprogrammingtips.com/java/how-to-generate-sha256-hash-in-java.html
	public String SHA256Hash( String in ) {
		try {
			MessageDigest digest = MessageDigest.getInstance( "SHA-256" );
			byte[] hash = digest.digest( in.getBytes() );
			return new String( hash );
		} catch( Exception e ) {
			System.out.println( "Error while hashing: " + e.toString() );
		}
		return null;
	}

	// encode string to base64
	public String Base64Encode( String in ) {
		String out = new String( Base64.getEncoder().encode( in.getBytes() ) );
		return out;
	}

	// decode string to base64
	public String Base64Decode( String in ) {
		String out = new String( Base64.getDecoder().decode( in.getBytes() ) );
		return out;
	}

	// The following two methods were found on https://howtodoinjava.com/security/aes-256-encryption-decryption
	// encrypt string with key
	public String EncryptString( String in, String key ) {
		try {
			byte[] iv = new byte[16];
			IvParameterSpec ivspec = new IvParameterSpec( iv );

			SecretKeyFactory factory = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA256" );
			KeySpec spec = new PBEKeySpec( key.toCharArray() );
			SecretKey tmp = factory.generateSecret( spec );
			SecretKeySpec secretKey = new SecretKeySpec( tmp.getEncoded(), "AES" );

			Cipher cipher = Cipher.getInstance( "AES/CBC/PKCS5Padding" );
			cipher.init( Cipher.ENCRYPT_MODE, secretKey, ivspec );
			return new String( Base64.getEncoder().encode( cipher.doFinal( in.getBytes() ) ) );
		} catch( Exception e ) {
			System.out.println( "Error while encrypting: " + e.toString() );
		}

		return null;
	}

	// decrypt string with key
	public String DecryptString( String in, String key ) {
		try {
			byte[] iv = new byte[16];
			IvParameterSpec ivspec = new IvParameterSpec( iv );

			SecretKeyFactory factory = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA256" );
			KeySpec spec = new PBEKeySpec( key.toCharArray() );
			SecretKey tmp = factory.generateSecret( spec );
			SecretKeySpec secretKey = new SecretKeySpec( tmp.getEncoded(), "AES" );

			Cipher cipher = Cipher.getInstance( "AES/CBC/PKCS5Padding" );
			cipher.init( Cipher.DECRYPT_MODE, secretKey, ivspec );
			return new String( Base64.getEncoder().encode( cipher.doFinal( in.getBytes() ) ) );
		} catch( Exception e ) {
			System.out.println( "Error while encrypting: " + e.toString() );
		}

		return null;
	}
}
