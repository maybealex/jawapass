// Gregory Dalla

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

// bottom bar, includes license button
public class BarPanel extends JPanel {
	public BarPanel() {
		// two columns
		this.setLayout( new GridLayout( 1, 2 ) );

		JButton licence = new JButton( "LICENCE" );
		licence.addActionListener( new LicenceButton() );

		this.add( licence );
		this.add( new BarPanelRight() );
	}

	private class LicenceButton implements ActionListener {
		public void actionPerformed( ActionEvent e ) {
			JFrame licenceFrame = new LicenceFrame( "LICENCE" );
		}
	}
}

// right half of bar, contains button for new secret
class BarPanelRight extends JPanel {
	public BarPanelRight() {
		this.setLayout( new GridLayout( 1, 2 ) );

		JLabel label = new JLabel( "Add secret: " );
		JButton button = new JButton( "+" );
		button.addActionListener( new NewButton() );

		this.add( label );
		this.add( button );
	}

	private class NewButton implements ActionListener {
		public void actionPerformed( ActionEvent e ) {
			JFrame creation = new CreationFrame( "Add a secret" );
		}
	}
}
