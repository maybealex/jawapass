// Gregory Dalla

// class to store secrets
public class Secret {
	private int type;
	private String name, data;

	public Secret( int type, String name, String data ) {
		this.type = type;
		this.name = name;
		this.data = data;
	}

	public int getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getData() {
		return data;
	}
}
