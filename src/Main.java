// Gregory Dalla

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;

// main class. duh
public class Main {
	private static int screenWidth, screenHeight, windowWidth, windowHeight, windowX, windowY;
	public static void main( String[] args ) {
		// set up window
		// do some math
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		screenWidth = (int)screenSize.getWidth();
		screenHeight = (int)screenSize.getHeight();
		windowWidth = screenWidth / 3;
		windowHeight = (int)(screenHeight * .8);
		windowX = ( screenWidth / 2 ) - windowWidth;
		windowY = ( screenHeight - windowHeight ) / 2;

		// make that window
		JFrame window = new JFrame( "JawaPass" );
		window.setSize( windowWidth, windowHeight );
		window.setLocation( windowX, windowY );
		window.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		window.setVisible(true);

		// its time for to add some content
		window.setContentPane( new Content() );
	}

	// return window dimension data
	public static int[] getDims() {
		return new int[] { screenWidth, screenHeight, windowWidth, windowHeight, windowX, windowY };
	}
}
